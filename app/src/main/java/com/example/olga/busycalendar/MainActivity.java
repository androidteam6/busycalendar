package com.example.olga.busycalendar;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import java.io.IOException;



public class MainActivity extends AppCompatActivity {
    ImageView photo;
    EditText name;
    static final int GALLERY_REQUEST = 1;

      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        photo = (ImageView)findViewById(R.id.photo);
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

//          name = (EditText)findViewById(R.id.name);
//          name.setOnClickListener(new View.OnClickListener() {
//              @Override
//              public void onClick(View v) {
//
//              }
//          });
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent imageReturnedIntent){
        super.onActivityResult(requestCode,resultCode,imageReturnedIntent);

        Bitmap bitmap = null;
        ImageView photo = (ImageView)findViewById(R.id.photo);
        switch (requestCode){
            case GALLERY_REQUEST:
                if (resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),selectedImage);
                    }catch(IOException e){
                        e.printStackTrace();
                    }
                    photo.setImageBitmap(bitmap);
                }
        }
    }


}
